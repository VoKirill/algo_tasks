'''
Alice is playing an arcade game and wants to climb to the top of the leaderboard and wants to track her ranking. The game uses Dense Ranking, so its leaderboard works like this:

The player with the highest score is ranked number  on the leaderboard.
Players who have equal scores receive the same ranking number, and the next player(s) receive the immediately following ranking number.
For example, the four players on the leaderboard have high scores of , , , and . Those players will have ranks , , , and , respectively. If Alice's scores are ,  and , her rankings after each game are ,  and .

Function Description

Complete the climbingLeaderboard function in the editor below. It should return an integer array where each element  represents Alice's rank after the  game.

climbingLeaderboard has the following parameter(s):

scores: an array of integers that represent leaderboard scores
alice: an array of integers that represent Alice's scores
Input Format

The first line contains an integer , the number of players on the leaderboard.
The next line contains  space-separated integers , the leaderboard scores in decreasing order.
The next line contains an integer, , denoting the number games Alice plays.
The last line contains  space-separated integers , the game scores.
https://www.hackerrank.com/challenges/climbing-the-leaderboard/
'''

#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the climbingLeaderboard function below.
def place_one_score(scores, alise_result):
    beg = 0
    end = len(scores)-1

    while end-beg>0:
        check = int(((end+beg))//2)

        if check == 0:
           if alise_result>=scores[check]:
               return 1
           else:
               beg = 1

        elif check == len(scores)-1:
            if alise_result<scores[check]:
                return len(scores)+1
            elif alise_result == scores[check]:
                return len(scores)
            else:
                end = check-1

        else:
            if alise_result==scores[check]:
                return check+1
            elif alise_result>scores[check]:
                if end-beg>1:
                    end = check-1
                else:
                    end -=1
            elif alise_result<scores[check]:
                beg = check+1

    if beg==0:
        if alise_result>=scores[beg]:
            return 1
    else:
        if alise_result > scores[beg]:
            return beg+1
        elif alise_result == scores[beg]:
            return beg+1
        else:
            return beg+2

def climbingLeaderboard(scores, alice):
    scores = sorted(list(set(scores)), reverse = True)

    result = []
    for res in alice:
        result.append(place_one_score(scores, res))
    return result

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    scores_count = int(input())

    scores = list(map(int, input().rstrip().split()))

    alice_count = int(input())

    alice = list(map(int, input().rstrip().split()))

    result = climbingLeaderboard(scores, alice)

    fptr.write('\n'.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
