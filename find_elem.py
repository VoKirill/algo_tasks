
def find_point(a,b):
    '''

    даны два списка в списке есть элемент после котрого списки одинаковые, найти этот эдемент, сложность O(log(n))
    '''
    beg = 0
    end = len(a)-1
    while end-beg>0:
        pos = (beg+end)//2
        if a[pos]==b[pos] and a[pos-1]==b[pos-1]:
            end = pos-1
        elif a[pos]!=b[pos]:
            beg = pos+1
        elif a[pos]==b[pos] and a[pos-1]!=b[pos-1]:
            return pos
        
    if beg ==len(a)-1 and a[beg]!=b[beg]:
        return 'wrong'
    else:
        return beg




#a = [1,2,3,10,5]
#a = [10,7,9,4,5]
a = [1,1,1,1,1]
b = [7,8,9,4,5]

print(find_point(a,b))