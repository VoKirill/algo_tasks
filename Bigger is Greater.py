# Complete the biggerIsGreater function below.
'''
Lexicographical order is often known as alphabetical order when dealing with strings. A string is greater than another string if it comes later in a lexicographically sorted list.

Given a word, create a new word by swapping some or all of its characters. This new word must meet two criteria:

It must be greater than the original word
It must be the smallest word that meets the first condition
For example, given the word  w = abcd , the next largest word is  abdc.

Complete the function biggerIsGreater below to create and return the new string meeting the criteria. If it is not possible, return no answer.

Function Description

Complete the biggerIsGreater function in the editor below. It should return the smallest lexicographically higher string possible from the given string or no answer.

biggerIsGreater has the following parameter(s):

w: a string
Input Format

The first line of input contains , the number of test cases.
Each of the next  lines contains .


'''
def find_min_of_max(target, str_):
    answer = 'za'
    position = -1
    for i, letter in enumerate(str_):
        if  letter>target and letter<answer:
            answer = letter
            position = i
    if answer == 'za':
        return None
    else:
        return answer, position


def biggerIsGreater(w):
    w = list(w)

    if sorted(w, reverse=True) == w:
        return 'no answer'

    for i in range(-2, -1*len(w)-1, -1):
        min_of_max = find_min_of_max(w[i], w[i+1:])
        if min_of_max:
            break

    if not min_of_max:
        return 'no_answer'
    else:
        w[i],w[i+min_of_max[1]+1] = w[i+min_of_max[1]+1],w[i]
        right_part = sorted(w[i+1:])
        left_part = w[0:i+1]
        return ''.join(left_part+right_part)

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    T = int(input())

    for T_itr in range(T):
        w = input()

        result = biggerIsGreater(w)

        fptr.write(result + '\n')

    fptr.close()