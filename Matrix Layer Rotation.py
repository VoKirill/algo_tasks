'''
You are given a 2D matrix of dimension  and a positive integer . You have to rotate the matrix  times and print the resultant matrix. Rotation should be in anti-clockwise direction.

Rotation of a  matrix is represented by the following figure. Note that in one rotation, you have to shift elements by one step only.
https://www.hackerrank.com/challenges/matrix-rotation-algo/problem
'''
import math
import os
import random
import re
import sys

# Complete the matrixRotation function below.
def moveOneLayer(line,col,r, matrix, new_matrix):

    '''

    :param line:
    :param col:
    :param r:
    :param matrix:
    :param new_matrix:
    :return: new matrix with moved new layer
    '''
    x = line[0]
    y = col[0]

    round =2*(line[1]-line[0])+ 2*(col[1]-col[0]) #amount of the element in one layer
    r = r % round # real number of rotetion which we have to move
    while r!=0:
        if x == min(line) and y != min(col):
            y -= 1
            r -= 1
        elif x == max(line) and y != max(col):
            y += 1
            r -= 1
        elif y == max(col) and x != min(line):
            x -= 1
            r -= 1
        elif y == min(col) and x != max(line):
            x += 1
            r -= 1

    new_matrix[x][y] = matrix[line[0]][col[0]]

    sum_ = round-1 #amount of the element which we have to rotate in current iteration

    x1 = line[0]
    y1 = col[0]

    for i in range(sum_):

        if x1 == min(line) and y1!=max(col):
            y1+=1
        elif x1 == max(line) and y1!=min(col):
            y1 -= 1
        elif y1 == min(col) and x1 != min(line):
            x1 -= 1
        elif y1 == max(col) and x1 != max(line):
            x1 += 1


        if x == min(line) and y!=max(col):
            y+=1
        elif x == max(line) and y!=min(col):
            y -= 1
        elif y == min(col) and x != min(line):
            x -= 1
        elif y == max(col) and x != max(line):
            x += 1

        new_matrix[x][y] = matrix[x1][y1]

    return new_matrix


def matrixRotation(matrix, r):
    new_matrix = [[0 for _ in range(len(matrix[0]))] for i in range(len(matrix))]

    line = [0,len(matrix)-1]
    col = [0,len(matrix[0])-1]
    chet = min(line[1]-line[0],col[1]-col[0])

    while chet>=1:
        new_matrix = moveOneLayer(line, col, r, matrix,  new_matrix)
        line[0]+=1
        line[1]-=1
        col[0]+=1
        col[1]-=1
        chet = min(line[1] - line[0], col[1] - col[0])

    for line in new_matrix:
        print(' '.join([str(elem) for elem in line]))



if __name__ == '__main__':

    mnr = input().rstrip().split()

    m = int(mnr[0])

    n = int(mnr[1])

    r = int(mnr[2])

    matrix = []

    for _ in range(m):
        matrix.append(list(map(int, input().rstrip().split())))

    matrixRotation(matrix, r)
